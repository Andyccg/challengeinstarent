# Challenge 2

This is a REST API to get de data from a file in a JSON format. Is very flexible, you just need
to provide a file in a Json Array format, and then you can filter attributes and elements using the API.

Is part of the Challenge 2, created mainly to manage high volumes of data.

This API is a searcher of data, that allow get information in a JSON format filtering by attributes values.

The following list shows the libraries used to create the API:

* json-simple 3.1.0
* lucene 7.1.0
* mockito 3.9.0
* junit 4

The project is using Spring Boot 2.4.4 and Java 8+

## Configuration

There is only one parameter configurable, to change it modify the application.properties located in resources package.

`document.input.path=/home/challenge_input.json`

This parameter is the file path, that file should be in a JSON format.

## Input File

The file should represent a Json Array to be mapped correctly.

For example, the following file called input.json has a Json Array

`[ { "key": "value", "key2": "value2"}, { "key1": "value1"} 
]`

## Parameters

To call the API there are three optionals Query params:

* limit = limit for results, default value is 10
* fields = fields to be displayed for each element found, values are separated by comma
* [fieldName] = every field or attribute in the Json, can be used as filter

Let's see how test the API

## How to run

As a Spring Boot app, you can run the application as follows:

`java -jar challenge-0.0.1.jar`

## Request Examples

We will be using for the examples the following file:

example.json

`[ { "key": "value", "key2": "value2"}, { "key": "value1" }
]`

### Get All results

To get all elements of the file, we just need to call the API without parameters (parameter limit is 10 by default)

http://localhost:8080/api/elements

Response:
```json 
{
    "data": [
        {
            "key2": "value2",
            "key": "value"
        },
        {
            "key": "value1"
        }
    ]
}
```
### Limit of results

To limit the number of elements in the response, we need to use the **limit** parameter.

http://localhost:8080/api/elements?limit=1

Response:
```json 
{
    "data": [
        {
            "key2": "value2",
            "key": "value"
        }
    ]
}
```

### Get part of each element

We can also get just the data that we need, this is possible thanks to fields parameter.
The values should be separated by comma, for example:

fields=key,key2

If the parameter is not present in the request, the response will contain all the attributes.

Request Example:

http://localhost:8080/api/elements?fields=key

Response:
```json 
{
    "data": [
        {
            "key": "value"
        },
        {
            "key": "value1"
        }
    ]
}
```

### Filter by attribute value

We can filter using all attributes of the element, is possible use more than one attribute to filter at the same time.
The filter will be based on the content of the value.

Example:

http://localhost:8080/api/elements?key=value

In this case we are getting all the elements that have an attribute called **key**, and a value for this attribute that contains **value**

Response:
```json 
{
    "data": [
        {
            "key2": "value2",
            "key": "value"
        }
    ]
}
```

## Try out!

You can see more examples and documentation [here](https://documenter.getpostman.com/view/7026999/TzJoDKzc).
Try by yourself and play using postman.
