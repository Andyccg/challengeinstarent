package com.instarent.challenge.manager;

import com.instarent.challenge.exceptions.SystemException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
public class DocumentManagerTest {

    @Test
    public void shouldLoadTwoDocumentsWhenTheJsonFileContainsTwoJson() throws SystemException, IOException {
        String testFileName = "challenge_test.json";
        DocumentManager documentManager = new DocumentManager(testFileName);

        IndexReader indexReader = DirectoryReader.open(documentManager.getMemoryIndex());

        Assert.assertEquals("Number of elements loaded", 2, indexReader.numDocs());
    }

    @Test(expected = SystemException.class)
    public void shouldThrowExceptionWhenTheFileToLoadDoesNotExist() throws SystemException, IOException {
        String testFileName = "file_no_exists.json";
        DocumentManager documentManager = new DocumentManager(testFileName);
    }
}
