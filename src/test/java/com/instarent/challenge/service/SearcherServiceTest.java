package com.instarent.challenge.service;

import com.instarent.challenge.exceptions.SystemException;
import com.instarent.challenge.manager.DocumentManager;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class SearcherServiceTest {

    @Mock
    DocumentManager documentManager;

    @InjectMocks
    SearcherService searcherService;

    Directory directory;
    Analyzer analyzer;

    @Before
    public void setup() throws IOException {
        directory = new RAMDirectory();
        analyzer = new SimpleAnalyzer();
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
        IndexWriter writer = new IndexWriter(directory, indexWriterConfig);
        writer.close();
    }

    @Test
    public void shouldGetNoResultWhenThereAreNotJsonIndexed() throws SystemException, IOException {
        Map<String, String> filters = new HashMap<>();
        int limit = 1;
        List<String> fields = null;
        when(documentManager.getMemoryIndex()).thenReturn(directory);

        List<Map<String, String>> response = searcherService.multiSearch(filters, limit, fields);

        Assert.assertEquals("Response Item counter is 0", 0, response.size());
    }

    @Test
    public void shouldGetDocumentWithFieldRequestedWhenThereAreNotFilters() throws SystemException, IOException {
        Map<String, String> filters = new HashMap<>();
        int limit = 10;
        List<String> fields = Lists.list("key2");
        when(documentManager.getMemoryIndex()).thenReturn(directory);
        Document doc = new Document();
        doc.add(new TextField("key","value", Field.Store.YES));
        doc.add(new TextField("key2","value2", Field.Store.YES));

        addOneDocument(doc);

        List<Map<String, String>> response = searcherService.multiSearch(filters, limit, fields);

        Assert.assertEquals("Response Item counter is 1", 1, response.size());
        Assert.assertEquals("Just one field returned", 1, response.get(0).size());
        Assert.assertEquals("Field returned", "value2", response.get(0).get("key2"));
    }

    @Test
    public void shouldGetOnlyDocumentsThatMatchWithFiltersWhenThereAreFilters() throws SystemException, IOException {
        Map<String, String> filters = new HashMap<>();
        filters.put("key", "second");
        int limit = 10;
        List<String> fields = null;
        when(documentManager.getMemoryIndex()).thenReturn(directory);
        when(documentManager.getAnalyzer()).thenReturn(analyzer);
        Document doc = new Document();
        doc.add(new TextField("key","first", Field.Store.YES));
        Document doc2 = new Document();
        doc2.add(new TextField("key","second", Field.Store.YES));

        addDocuments(Lists.newArrayList(doc,doc2));

        List<Map<String, String>> response = searcherService.multiSearch(filters, limit, fields);

        Assert.assertEquals("Response Item counter is 1", 1, response.size());
        Assert.assertEquals("Field returned", "second", response.get(0).get("key"));
    }

    @Test(expected = SystemException.class)
    public void shouldReturnExceptionWhenMemoryIndexIsNotComplete() throws SystemException, IOException {
        Map<String, String> filters = new HashMap<>();
        int limit = 10;
        when(documentManager.getMemoryIndex()).thenReturn(new RAMDirectory());

        List<Map<String, String>> response = searcherService.multiSearch(filters, limit, null);
    }


    private void addOneDocument(Document document) throws IOException {
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
        IndexWriter writer = new IndexWriter(directory, indexWriterConfig);
        writer.addDocument(document);
        writer.close();
    }

    private void addDocuments(List<Document> documents) throws IOException {
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
        IndexWriter writer = new IndexWriter(directory, indexWriterConfig);
        writer.addDocuments(documents);
        writer.close();
    }
}
