package com.instarent.challenge.controller;

import com.instarent.challenge.exceptions.SystemException;
import com.instarent.challenge.response.ServiceResponse;
import com.instarent.challenge.service.SearcherService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class SearcherControllerTest {

    @Mock
    SearcherService searcherService;

    @InjectMocks
    SearcherController searcherController;

    @Test
    public void shouldGetItemListWhenParametersAreCorrect() throws SystemException {
        List<Map<String, String>> mockResponse = new ArrayList<>();
        Map<String, String> elementResponse = new HashMap<>();
        elementResponse.put("attributeOne", "valueOne");
        elementResponse.put("attributeTwo", "valueTwo");
        mockResponse.add(elementResponse);
        when(searcherService.multiSearch(anyMap(), anyInt(), anyList())).thenReturn(mockResponse);

        ResponseEntity<ServiceResponse> response = searcherController.search(new ArrayList<>(), 10, new HashMap<>());
        List<Map<String, String>> dataResponse = response.getBody().getData();

        Assert.assertEquals("Item number", mockResponse.size(), dataResponse.size());
        Assert.assertEquals("Validate First Attribute",
                mockResponse.get(0).get("attributeOne"), dataResponse.get(0).get("attributeOne"));
        Assert.assertEquals("Validate Second Attribute",
                mockResponse.get(0).get("attributeTwo"), dataResponse.get(0).get("attributeTwo"));
    }

    @Test
    public void shouldGetAnErrorWhenServiceThrowException() throws SystemException {
        when(searcherService.multiSearch(anyMap(), anyInt(), anyList())).thenThrow(new SystemException("Error"));

        ResponseEntity<ServiceResponse> response = searcherController.search(new ArrayList<>(), 10, new HashMap<>());
        List<Map<String, String>> dataResponse = response.getBody().getData();

        Assert.assertNull(dataResponse);
        Assert.assertEquals("Error code", 500, response.getStatusCode().value());
        Assert.assertEquals("Error message", "Error", response.getBody().getMessage());
    }

}
