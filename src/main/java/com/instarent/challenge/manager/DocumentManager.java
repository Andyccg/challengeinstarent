package com.instarent.challenge.manager;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;
import com.instarent.challenge.exceptions.SystemException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class DocumentManager {

    private static final Logger logger = LoggerFactory.getLogger(DocumentManager.class);

    private final StandardAnalyzer analyzer;
    private final Directory memoryIndex;


    public DocumentManager(@Value("${document.input.path}") String filePath) throws SystemException {
        analyzer = new StandardAnalyzer();
        memoryIndex = new RAMDirectory();
        List<Document> documentList = new ArrayList<>();

        JsonArray deserializedJson = null;

        try (FileReader fileReader = new FileReader(filePath)) {
            deserializedJson = (JsonArray) Jsoner.deserialize(fileReader);
        } catch (Exception e) {
            logger.error("There is a problem loading the json file: {}", e.getMessage());
            throw new SystemException("Error loading json file");
        }

        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);

        try (IndexWriter writter = new IndexWriter(memoryIndex, indexWriterConfig)) {
            deserializedJson.forEach(element -> addDocument((JsonObject) element, documentList));
            writter.addDocuments(documentList);
        } catch (IOException e) {
            logger.error("There is a problem creating elements from json: {}", e.getMessage());
            throw new SystemException("Error loading json file");
        }

    }


    private void addDocument(JsonObject jsonObject, List<Document> documentList) {
        Document document = new Document();
        for(Map.Entry<String, Object> entry : jsonObject.entrySet()) {
            document.add(new TextField(entry.getKey(), entry.getValue().toString(), Field.Store.YES));
        }
        documentList.add(document);
    }


    public Directory getMemoryIndex() {
        return memoryIndex;
    }


    public Analyzer getAnalyzer() {
        return analyzer;
    }

}
