package com.instarent.challenge.exceptions;

import org.springframework.http.HttpStatus;

public class SystemException extends BaseException{

    public SystemException(String message) {
        setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        setMessage(message);
    }
}
