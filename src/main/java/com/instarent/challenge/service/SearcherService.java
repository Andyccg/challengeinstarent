package com.instarent.challenge.service;

import com.instarent.challenge.exceptions.SystemException;
import com.instarent.challenge.manager.DocumentManager;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SearcherService {

    private static final Logger logger = LoggerFactory.getLogger(SearcherService.class);

    @Autowired
    private DocumentManager documentManager;


    public List<Map<String, String>> multiSearch(Map<String, String> filters, int limit, List<String> fieldsRequested) throws SystemException {
        try {
            Query finalQuery = getQuery(filters);
            IndexReader indexReader = DirectoryReader.open(documentManager.getMemoryIndex());
            IndexSearcher searcher = new IndexSearcher(indexReader);
            TopDocs topDocs = searcher.search(finalQuery, limit);
            return createElements(fieldsRequested, searcher, topDocs);
        } catch (IOException | ParseException e) {
            logger.error("Error searching: {}", e.getStackTrace());
            throw new SystemException("Error creating search");
        }
    }


    private List<Map<String, String>> createElements(List<String> fieldsRequested, IndexSearcher searcher, TopDocs topDocs) throws IOException {
        List<Map<String, String>> elements = new ArrayList<>();
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Map<String, String> element = new HashMap<>();
            Document document = searcher.doc(scoreDoc.doc);
            for(IndexableField field : searcher.doc(scoreDoc.doc).getFields()) {
                if(fieldsRequested != null) {
                    if(fieldsRequested.contains(field.name())) {
                        element.put(field.name(), document.get(field.name()));
                    }
                } else {
                    element.put(field.name(), document.get(field.name()));
                }
            }
            elements.add(element);
        }
        return elements;
    }


    private Query getQuery(Map<String, String> filters) throws ParseException {
        if(filters.isEmpty()) {
            return new MatchAllDocsQuery();
        } else {
            BooleanQuery.Builder groupQuery = new BooleanQuery.Builder();
            for (Map.Entry<String, String> entry : filters.entrySet()) {
                QueryParser queryParser = new QueryParser(entry.getKey(), documentManager.getAnalyzer());
                Query query = queryParser.parse(filters.get(entry.getKey()));
                groupQuery.add(query, BooleanClause.Occur.MUST);
            }
            return groupQuery.build();
        }
    }


}
