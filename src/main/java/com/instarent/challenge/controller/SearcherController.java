package com.instarent.challenge.controller;

import com.instarent.challenge.exceptions.SystemException;
import com.instarent.challenge.response.ServiceResponse;
import com.instarent.challenge.service.SearcherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/elements")
public class SearcherController {

    private static final Logger logger = LoggerFactory.getLogger(SearcherController.class);

    @Autowired
    private SearcherService searcherService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ServiceResponse> search(@RequestParam(required = false) List<String> fields,
                                         @RequestParam(required = false, defaultValue = "10") int limit,
                                         @RequestParam(required = false) Map<String, String> params) {

        logger.info("Starting searching");
        params.remove("fields");
        params.remove("limit");
        ServiceResponse serviceResponse = new ServiceResponse();
        try {
            List<Map<String, String>> response = searcherService.multiSearch(params, limit, fields);
            serviceResponse.setData(response);
            logger.info("Returning {} items", response.size());
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        } catch (SystemException e) {
            serviceResponse.setMessage(e.getMessage());
            return new ResponseEntity<>(serviceResponse, HttpStatus.valueOf(e.getCode()));
        }
    }

}
